package com.zuitt.activity.services;

import com.zuitt.activity.models.User;
import org.springframework.http.ResponseEntity;

public interface UserService {
    // C- Create
    void createUser(User user);

    // R- Retrieve/Read
    Iterable<User> getUsers();

    // U- Update
    ResponseEntity updateUser(Long id, User user);

    // D- Delete
    ResponseEntity deleteUser(Long id);

}

